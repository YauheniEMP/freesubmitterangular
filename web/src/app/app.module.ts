import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { UploadMainComponent } from './components/upload/uploadmain/uploadmain.component';
import { SubmitMainComponent } from './components/submit/submitmain/submitmain.component';
import { LoginComponent } from './components/authorization/login/login.component';
import { AuthGuard } from 'src/app/guard/auth.guard';
import { AccountService } from 'src/app/services/account.service';
import { HomeComponent } from './components/home/home.component';
import { SubmitItemComponent } from './components/submit/submititem/submititem.component';
import { UploadItemComponent } from './components/upload/uploaditem/uploaditem.component';
import { SettingsMainComponent } from './components/settings/settingsmain/settingsmain.component';
import { HistoryMainComponent } from './components/history/historymain/historymain.component';
import { HistoryitemComponent } from './components/history/historyitem/historyitem.component';
import { ImageInfoComponent } from './components/history/imageinfo/imageinfo.component';
import { ConfirmComponent } from './components/authorization/confirm/confirm.component';
import { RecoverypasswordComponent } from './components/authorization/recoverypassword/recoverypassword.component';
import { ChangepasswordComponent } from './components/authorization/changepassword/changepassword.component';
import { SettingsItemComponent } from './components/settings/settingsitem/settingsitem.component';
import { InProcessComponent } from './components/history/inprocess/inprocess.component';
import { SubmittedComponent } from './components/history/submitted/submitted.component';
import { FooterComponent } from './components/footer/footer.component';
import { LoginAuthComponent } from './components/auth/login/login.component';
import { LayoutComponent } from './components/layout/layout.component';
import { UploadingComponent } from './components/upload/uploading/uploading.component';
import { MaterialModule } from './material/material.module';
import { SideBarComponent } from './components/layout/side-bar/side-bar.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { RegisterSuccessComponent } from './components/auth/register-success/register-success.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    UploadMainComponent,
    SubmitMainComponent,
    RegisterComponent,
    LoginComponent,
    HomeComponent,
    SubmitItemComponent,
    UploadItemComponent,
    SettingsMainComponent,
    HistoryMainComponent,
    HistoryitemComponent,
    ImageInfoComponent,
    ConfirmComponent,
    RecoverypasswordComponent,
    ChangepasswordComponent,
    SettingsItemComponent,
    InProcessComponent,
    SubmittedComponent,
    FooterComponent,
    LoginAuthComponent,
    LayoutComponent,
    UploadingComponent,
    SideBarComponent,
    RegisterSuccessComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
  ],
  providers: [AuthGuard, AccountService],
  bootstrap: [AppComponent]
})
export class AppModule { }
