import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthdataService {

  private subject = new Subject<any>();
  constructor() { }

  setData(name: string, isAuth: boolean, isConfirmed: boolean) {
    this.subject.next({ name, isAuth, isConfirmed });
  }

  getData() {
    return this.subject.asObservable();
  }

}
