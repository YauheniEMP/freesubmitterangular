import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from './account.service';

@Injectable({
  providedIn: 'root'
})
export class GuardService {

  constructor(private router: Router, private accountService: AccountService) { }

  async isAuthenticated() {
    const token = await this.accountService.token();
    if (token.email !== 'default') {
     return true;
      } else {
      this.router.navigate(['']);
    }
  }
}
