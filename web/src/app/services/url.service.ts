import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UrlService {
  readonly vsDebugBaseUrl: string = 'https://localhost:5001';
  readonly iisExpressBaseUrl: string = 'https://localhost:44318';
  readonly iisLocalBaseUrl: string = 'http://api.isubmitter.loc';
  readonly remoteUrl: string = 'https://api.isubmitter.net';
  url: string;
  constructor() {
    this.url = this.vsDebugBaseUrl;
  }
}
