import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UrlService } from './url.service';
import { SimpleResultModel } from '../models/simpleresultmodel';
import { UploadingImage } from '../models/uploadingImage';

@Injectable({
  providedIn: 'root'
})
export class FilesService {
  baseUrl: string;
  constructor(private http: HttpClient, private url: UrlService) {
    this.baseUrl = this.url.url;
  }
  loadingFiles(): Promise<UploadingImage[]> {
    return this.http.get<UploadingImage[]>(`${this.baseUrl}/api/Files/LoadingFiles`, { withCredentials : true }).toPromise();
  }
  uploadFiles(files: File[]): Promise<any> {
    if (files === null || files === undefined || files.length === 0) {
      return;
    }
    const data: FormData = new FormData();
    files.forEach(p => data.append('file', p, p.name));
    return this.http.post<any>(`${this.baseUrl}/api/Upload/UploadFiles`, data,
      { withCredentials: true }).toPromise();
  }

  deleteImages(ids: string[]): Promise<SimpleResultModel> {
    const url = `${this.baseUrl}/api/Files/DeleteImages/`;
    const data = {
      ids
    };
    return this.http.post<SimpleResultModel>(url, data, { withCredentials: true }).toPromise();
  }

  resubmitFile(id: string, stock: string): Promise<any> {
    const url = `${this.baseUrl}/api/Submit/SubmitImage/`;
    return this.http.post(url,  { id, stock }, { withCredentials: true }).toPromise();
  }
}
