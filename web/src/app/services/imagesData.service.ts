import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SubmitItem } from '../models/submititem';
import { UrlService } from './url.service';
import { Observable } from 'rxjs';
import { LastSubmittedImagesModel } from '../models/lastsubmittedimagesmodel';
import { ProcessingImagesModel } from '../models/processingimagesmodel';
import { NotSubmittedImagesModel } from '../models/notsubmittedimagesmodel';
import { SimpleResultModel } from '../models/simpleresultmodel';
import { TitleModel } from '../models/titlemodel';
import { KeywordsModel } from '../models/keywordsmodel';
import { PaginatedList } from '../models/paginatedList';
import { Image } from '../models/image';
@Injectable({
  providedIn: 'root'
})
export class ImagesDataService {
  baseUrl: string;
  constructor(private http: HttpClient, private url: UrlService) {
    this.baseUrl = this.url.url;
  }

  getLastImages(page: number = 1, pageSize: number = 50): Promise<PaginatedList<Image>> {
    const url = `${this.baseUrl}/api/Files/SubmittedImages?page=${page}&pageSize=${pageSize}`;
    return this.http.get<PaginatedList<Image>>(url, { withCredentials: true }).toPromise();
  }

  getProcessingImages(page: number = 1, pageSize: number = 50): Promise<PaginatedList<Image>> {
    const url = `${this.baseUrl}/api/Files/ProcessingImages?page=${page}&pageSize=${pageSize}`;
    return this.http.get<PaginatedList<Image>>(url, { withCredentials: true }).toPromise();
  }

  getNotSubmittedImages(page: number = 1, pageSize: number = 50): Promise<PaginatedList<SubmitItem>> {
    const url = `${this.baseUrl}/api/Files/UploadedImages?page=${page}&pageSize=${pageSize}`;
    return this.http.get<PaginatedList<SubmitItem>>(url, { withCredentials: true }).toPromise();
  }

  updateImages(items: SubmitItem[]): Promise<SimpleResultModel> {
    const data = {
      items
    };
    const url = `${this.baseUrl}/api/ImageData/UpdateImage/`;
    return this.http.post<SimpleResultModel>(url, data, { withCredentials: true }).toPromise();
  }

  submitImages(ids: string[]): Promise<any> {
    const url = `${this.baseUrl}/api/Submit/SubmitImages/`;
    return this.http.post(url, ids, { withCredentials: true }).toPromise();
  }


  getPreview(id: string): Observable<Blob> {
    const qid = `?id=${id}`;
    const url = `${this.baseUrl}/api/ImageData/GetPreview/`;
    return this.http.get(`${url}${qid}`, { withCredentials: true, responseType: 'blob' });
  }

  getTitle(id: string): Promise<TitleModel> {
    const url = `${this.baseUrl}/api/ImageData/GetTitle/`;
    return this.http.get<TitleModel>(`${url}${id}`, { withCredentials: true }).toPromise();
  }

  getKeywords(id: string): Promise<KeywordsModel> {
    const url = `${this.baseUrl}/api/ImageData/GetKeywords/`;
    return this.http.get<KeywordsModel>(`${url}${id}`, { withCredentials: true }).toPromise();
  }

  setTitle(id: string, title: string): Promise<any> {
    const url = `${this.baseUrl}/api/ImageData/SetTitle/`;
    const data = {
      id,
      title
    };
    return this.http.post(url, data, { withCredentials: true }).toPromise();
  }

  setKeywords(id: string, keywords: string[]): Promise<any> {
    const url = `${this.baseUrl}/api/ImageData/SetKeywords/`;
    const data = {
      id,
      keywords
    };
    return this.http.post(url, data, { withCredentials: true }).toPromise();
  }
}
