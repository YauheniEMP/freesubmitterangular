import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from 'src/app/models/user';
import { StockData } from 'src/app/models/stockdata';
import { RegisterUser } from '../components/authorization/register/register.component';
import { UrlService } from './url.service';
import { TokenModel } from '../models/tokenmodel';
import { LoginResultModel } from '../models/loginresultmodel';
import { StockDataResultModel } from '../models/stockdataresultmodel';
import { SimpleResultModel } from '../models/simpleresultmodel';
import { ResultModel } from '../models/resultmodel';
@Injectable({
  providedIn: 'root'
})
export class AccountService {

  baseUrl: string;
  constructor(private http: HttpClient, private url: UrlService) {
    this.baseUrl = this.url.url;
  }

  token(): Promise<TokenModel> {
    return this.http.get<TokenModel>(`${this.baseUrl}/api/Accounts/Token`, { withCredentials: true }).toPromise();
  }

  login(user: User): Promise<LoginResultModel> {
    return this.http.post<LoginResultModel>(`${this.baseUrl}/api/Accounts/Login`, user, { withCredentials: true }).toPromise();
  }

  register(user: RegisterUser): Promise<ResultModel> {
    return this.http.post<ResultModel>(`${this.baseUrl}/api/Accounts/Register`, user, { withCredentials: true })
      .toPromise();
  }

  logoutUser(): Promise<any> {
    return this.http.get(`${this.baseUrl}/api/Accounts/Logout`, { withCredentials: true }).toPromise();
  }

  stockData(): Promise<StockDataResultModel> {
    return this.http.get<StockDataResultModel>(`${this.baseUrl}/api/StockData/StockData`, { withCredentials: true })
      .toPromise();
  }

  saveStockData(stockData: StockData[]): Promise<SimpleResultModel> {
    const data = stockData;
    return this.http.post<SimpleResultModel>(`${this.baseUrl}/api/StockData/StockData`,
      data, { withCredentials: true }).toPromise();
  }

  confirmEmail(email: string, code: string): Promise<SimpleResultModel> {
    const data = {
      email,
      code
    };
    return this.http.post<SimpleResultModel>(`${this.baseUrl}/api/Accounts/ConfirmEmail`, data, { withCredentials: true })
      .toPromise();
  }

  sendRecoveryEmail(email: string): Promise<ResultModel> {
    const data = {
      email
    };
    return this.http.post<ResultModel>(`${this.baseUrl}/api/Accounts/RecoveryPasswordEmail`, data).toPromise();
  }

  setNewPassword(email: string, code: string, password: string, confirmPassword: string): Promise<SimpleResultModel> {
    const data = {
      email,
      code,
      password,
      confirmPassword
    };
    return this.http.post<SimpleResultModel>(`${this.baseUrl}/api/Accounts/ResetPassword`, data).toPromise();
  }

  saveStockItem(stockData: StockData): Promise<SimpleResultModel> {
    const data = stockData;
    return this.http.post<SimpleResultModel>(`${this.baseUrl}/api/StockData/StockItem`, data, { withCredentials: true })
      .toPromise();
  }
}
