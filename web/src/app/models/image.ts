import { ImageStatus } from './imagestatus';

export class Image {
    name: string;
    id: string;
    title: string;
    keywords: string;
    statusList: ImageStatus[];
    isChecked: boolean;
    submitDateTime: Date;
}
