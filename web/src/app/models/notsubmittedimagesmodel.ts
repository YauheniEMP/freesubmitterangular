import { SubmitItem } from './submititem';

export class NotSubmittedImagesModel {
    files: SubmitItem[];
}
