import { Image } from './image';
export class LastSubmittedImagesModel {
    images: Image[];
    pages: number;
}
