export class PaginatedList <T> {
    result: T[];
    totalCount: number;
    takenCount: number;
    skippedCount: number;
}
