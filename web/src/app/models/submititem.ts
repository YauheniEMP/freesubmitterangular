export class SubmitItem {
    isChecked: boolean;
    id: string;
    name: string;
    title: string;
    keywords: string[];
}
