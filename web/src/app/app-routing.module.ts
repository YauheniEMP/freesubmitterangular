import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UploadMainComponent } from './components/upload/uploadmain/uploadmain.component';
import { SubmitMainComponent } from './components/submit/submitmain/submitmain.component';
import { LoginComponent } from './components/authorization/login/login.component';
import { AuthGuard } from './guard/auth.guard';
import { HomeComponent } from './components/home/home.component';
import { SettingsMainComponent } from './components/settings/settingsmain/settingsmain.component';
import { HistoryMainComponent } from './components/history/historymain/historymain.component';
import { ImageInfoComponent } from './components/history/imageinfo/imageinfo.component';
import { ConfirmComponent } from './components/authorization/confirm/confirm.component';
import { RecoverypasswordComponent } from './components/authorization/recoverypassword/recoverypassword.component';
import { ChangepasswordComponent } from './components/authorization/changepassword/changepassword.component';
import { InProcessComponent } from './components/history/inprocess/inprocess.component';
import { SubmittedComponent } from './components/history/submitted/submitted.component';
import { LayoutComponent } from './components/layout/layout.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { RegisterSuccessComponent } from './components/auth/register-success/register-success.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    pathMatch: 'full'
  },
  {
    path: 'account', component: LayoutComponent,
    children: [
      { path: '', redirectTo: 'upload', pathMatch: 'full' },
      { path: 'upload', component: UploadMainComponent },
      { path: 'confirm', component: ConfirmComponent },
      { path: 'imageinfo/:name', component: ImageInfoComponent },
      { path: 'settings', component: SettingsMainComponent },

      {
        path: 'history', component: HistoryMainComponent, children: [
          { path: 'inprocess', component: InProcessComponent },
          { path: '', redirectTo: 'submitted', pathMatch: 'full' },
          { path: 'submitted', component: SubmittedComponent }
        ]
      },
      {
        path: 'submit',
        component: SubmitMainComponent
      },
    ]
  },
  {
    path: 'register',
    component: RegisterComponent,
  },
  {
    path: 'register-success',
    component: RegisterSuccessComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'recovery',
    component: RecoverypasswordComponent
  },
  {
    path: 'changepassword',
    component: ChangepasswordComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
