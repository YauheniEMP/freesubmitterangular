import { CanActivate, UrlTree, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AccountService } from 'src/app/services/account.service';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router, private accountService: AccountService) { }

    canActivate(route: ActivatedRouteSnapshot,
                state: RouterStateSnapshot): boolean {

        const x = this.accountService.token().then(data => {
            if (data.email === 'default') {

                this.router.navigate(['/login']);
            } else {
                return data;
            }
        }

        );
        return true;

    }

}
