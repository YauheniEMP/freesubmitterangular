import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSidenavModule, MatInputModule, MatButtonModule, MatSelectModule, MatIconModule} from '@angular/material';
import { MatCardModule} from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import {MatChipsModule} from '@angular/material/chips';
import {MatSnackBarModule} from '@angular/material/snack-bar';
@NgModule({
  declarations: [],
  imports: [
    MatToolbarModule,
    MatSelectModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    MatSidenavModule,
    MatProgressSpinnerModule,
    MatTableModule,
    CommonModule,
    MatListModule,
    MatChipsModule,
    MatIconModule,
    MatSnackBarModule
  ],
  exports: [
    MatToolbarModule,
    MatSelectModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    MatSidenavModule,
    MatProgressSpinnerModule,
    MatTableModule,
    CommonModule,
    MatListModule,
    MatChipsModule,
    MatIconModule,
    MatSnackBarModule
  ]
})
export class MaterialModule { }
