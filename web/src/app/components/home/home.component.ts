import { Component, OnInit } from '@angular/core';
import { GuardService } from 'src/app/services/guard.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  constructor(private guard: GuardService, private router: Router) { }
  ngOnInit() {
    this.guard.isAuthenticated().then(x => {
        if (x) {
          this.router.navigate(['account']);
        }
    });
  }
}
