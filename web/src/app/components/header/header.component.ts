import { Component, OnInit, OnDestroy } from '@angular/core';
import { AccountService } from 'src/app/services/account.service';
import { Router } from '@angular/router';
import { AuthdataService } from 'src/app/services/authdata.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  name: string;
  subscription: Subscription;
  isAuth: boolean;
  emailConfirmed: boolean;
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  constructor(private accountService: AccountService, private router: Router, private auth: AuthdataService) {
  }
  ngOnInit() {
    this.subscription = this.auth.getData().subscribe(data => {
      this.name = data.name;
      this.isAuth = data.isAuth;
      this.emailConfirmed = data.isConfirmed;
    });
    this.initialize();
  }

  async initialize() {
    const data = await this.accountService.token();
    this.auth.setData(data.email, data.email !== 'default', data.emailConfirmed);
  }

  logout() {
    this.auth.setData('', false, false);
    this.accountService.logoutUser().then(() => {
      this.router.navigate(['']);
    });
  }
}
