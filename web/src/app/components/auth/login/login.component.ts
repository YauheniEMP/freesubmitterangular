import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { User } from 'src/app/models/user';
import { AccountService } from 'src/app/services/account.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginAuthComponent implements OnInit {
  form: FormGroup;
  constructor(private fb: FormBuilder, private service: AccountService, private router: Router) { }

  ngOnInit() {
    this.form = this.fb.group({
      email : ['', [Validators.required, Validators.email]],
      password : ['', [Validators.required]]
    });
  }

  submit() {
    if (this.form.valid) {
      const user = new User();
      user.email = this.form.get('email').value;
      user.password = this.form.get('password').value;
      this.service.login(user).then( res => {
        if (res.result) {
          this.router.navigate(['/account']);
        }
      });
    }
  }

  register() {
    this.router.navigate(['/register']);
  }
}
