import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AccountService } from 'src/app/services/account.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  constructor(private fb: FormBuilder, private accountService: AccountService, private router: Router) { }

  ngOnInit() {
    this.registerForm = this.fb.group({
      Email: ['', [Validators.required, Validators.email]],
      Password: ['', [Validators.required, Validators.minLength(4)]],
      ConfirmPassword: ['', Validators.required]
    }, { validator: this.comparePasswords });
    this.registerForm.reset();
  }
  comparePasswords(fg: FormGroup) {
    const confirmPasswordCtrl = fg.get('ConfirmPassword');
    if (confirmPasswordCtrl.errors == null || 'passwordMismatch' in confirmPasswordCtrl.errors) {
      if (confirmPasswordCtrl.value !== fg.get('Password').value) {
        confirmPasswordCtrl.setErrors({ passwordMismatch: true });
      } else {
        confirmPasswordCtrl.setErrors(null);
      }
    }
  }

  async regusterUser() {

    if (this.registerForm.get('Email').errors != null) {
    }
    if (this.registerForm.get('Password').errors != null) {
    }
    if (this.registerForm.get('ConfirmPassword').errors != null) {
    }
    const user = new RegisterUser();
    user.email = this.registerForm.value.Email;
    user.password = this.registerForm.value.Password;
    user.confirmPassword = this.registerForm.value.ConfirmPassword;
    await this.accountService.register(user).then(data => {
      if (data.result) {
        this.router.navigate(['register-success']);
      } else {
        debugger;
        this.registerForm.setErrors({'server-error' : data.error});
        let t = this.registerForm.errors['server-error'];
        debugger;
      }
    });
  }
}
export class RegisterUser {
  email: string;
  password: string;
  confirmPassword: string;
}

