import { Component, OnInit } from '@angular/core';
import { FilesService } from 'src/app/services/files.service';
import { SubmitItem } from 'src/app/models/submititem';
import { ImagesDataService } from 'src/app/services/imagesData.service';
import { GuardService } from 'src/app/services/guard.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-submitmain',
  templateUrl: './submitmain.component.html',
  styleUrls: ['./submitmain.component.css']
})
export class SubmitMainComponent implements OnInit {
  files: SubmitItem[] = [];
  checkAllButtonListener = false;
  ckeckedImagesCount = 0;

  hidden = true;
  constructor(private filesService: FilesService,
              private imagesService: ImagesDataService, private guard: GuardService, private snackBar: MatSnackBar) { }
  ngOnInit() {
    this.initialize();
  }

  async initialize() {
    await this.guard.isAuthenticated().then(res => {
      if (res) {
        this.hidden = false;
        this.imagesService.getNotSubmittedImages().then(data => {
              this.files = data.result;
              this.files.forEach(x => x.isChecked = false);
            });
      }
    });
  }

  deleteFiles() {
    if (this.ckeckedImagesCount !== 0) {
      const ids: string[] = [];
      this.files.filter(x => x.isChecked === true).forEach(p => ids.push(p.id));
      this.filesService.deleteImages(ids).then();
      this.snackBar.open(`${ids.length} file(s) deleted`, '', {
        duration: 2000,
      });
      this.files = this.files.filter(p => p.isChecked === false);
      this.ckeckedImagesCount = 0;
    } else {
      this.snackBar.open('No checked files', 'Fail', {
        duration: 2000,
      });
    }
  }

  async saveFiles() {
    if (this.ckeckedImagesCount !== 0) {
      await this.imagesService.updateImages(this.files.filter(x => x.isChecked === true));
      this.snackBar.open(this.ckeckedImagesCount + ' file(s) saved', '', {
        duration: 2000,
      });
    } else {
      this.snackBar.open('No checked files', 'Fail', {
        duration: 2000,
      });
    }
  }

  async submitFiles() {
    if (this.ckeckedImagesCount !== 0) {
      await this.imagesService.updateImages(this.files.filter(x => x.isChecked === true)).then(data => {
        if (data) {
          const ids: string[] = [];
          this.files.filter(x => x.isChecked === true).forEach(x => ids.push(x.id));
          this.files = this.files.filter(x => x.isChecked !== true);
          this.imagesService.submitImages(ids);
          this.snackBar.open(this.ckeckedImagesCount + ' file(s) submitted', '', {
            duration: 2000,
          });
          this.ckeckedImagesCount = 0;
        } else {
          this.snackBar.open('Error when updating images', 'Fail', {
            duration: 2000,
          });
        }
      });
    } else {
      this.snackBar.open('No checked files', 'Fail', {
        duration: 2000,
      });
    }
  }

  checkAllToggle() {
    this.checkAllButtonListener = !this.checkAllButtonListener;
    this.onCheckAllBoxChange();
  }

  changeCheckedImagesCount(e: any) {
    if (e) {
      this.ckeckedImagesCount += 1;
    } else {
      this.ckeckedImagesCount -= 1;
    }
    if (this.files !== undefined && this.ckeckedImagesCount === this.files.length) {
      this.checkAllButtonListener = true;
    }
    if (this.files !== undefined && this.ckeckedImagesCount === 0) {
      this.checkAllButtonListener = false;
    }
  }

  onCheckAllBoxChange() {
    if (this.checkAllButtonListener === false) {
      this.ckeckedImagesCount = 0;
      this.files.forEach(x => x.isChecked = false);
    } else {
      this.files.forEach(x => x.isChecked = true);
      this.ckeckedImagesCount = this.files.length;
    }
  }

  fileTitleChanged(e: SubmitItem) {
    this.files.filter(x => x.isChecked === true && x.name !== e.name).forEach(p => p.title = e.title);
  }
  clearKeywords(e: boolean) {
    this.files.filter(p => p.isChecked === true).forEach(t => t.keywords = []);
  }

  deleteKeyword(e: string) {
    this.files.filter(p => p.isChecked === true).forEach(t => t.keywords = t.keywords.filter(o => o !== e));
  }
  newKeywordsAdded(e: string[]) {
    const files = this.files.filter(p => p.isChecked === true);
    e.forEach(key => {
      files.forEach(t => {
        if (t.keywords != null && t.keywords !== undefined && !t.keywords.includes(key) && t.keywords.length < 80) {
          t.keywords.push(key);
        }
        if (t.keywords === undefined || t.keywords == null) {
          t.keywords = [key];
        }
      }
      );
    });
  }

  fileCheckBoxChanged(e: SubmitItem) {
    if (e.isChecked === true) {
      this.ckeckedImagesCount += 1;
    } else {
      this.ckeckedImagesCount -= 1;
    }

    if (this.ckeckedImagesCount === this.files.length) {
      this.checkAllButtonListener = true;
    }
    if (this.ckeckedImagesCount === 0) {
      this.checkAllButtonListener = false;
    }
  }
}
