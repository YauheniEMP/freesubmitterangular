import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { SubmitItem } from 'src/app/models/submititem';
import { ImagesDataService } from 'src/app/services/imagesData.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-submititem',
  templateUrl: './submititem.component.html',
  styleUrls: ['./submititem.component.css']
})
export class SubmitItemComponent implements OnInit {

  @Input() fileItem: SubmitItem;
  @Output() itemTitle: EventEmitter<SubmitItem> = new EventEmitter();
  @Output() keywordsClear: EventEmitter<boolean> = new EventEmitter();
  @Output() deleteKeyword: EventEmitter<string> = new EventEmitter();
  @Output() itemCheck: EventEmitter<SubmitItem> = new EventEmitter();
  @Output() newKeywords: EventEmitter<string[]> = new EventEmitter();

  imageToShow: any;
  imageToShowSafe: SafeUrl;

  textContent = '';

  constructor(private sanitizer: DomSanitizer,
              private imagesDataService: ImagesDataService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.imagesDataService.getPreview(this.fileItem.id).subscribe(data => {
      this.createImageFromBlob(data);
    });
  }

  createImageFromBlob(image: Blob) {
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      this.imageToShow = reader.result;
      this.imageToShowSafe = this.sanitizer.bypassSecurityTrustUrl(this.imageToShow);
    }, false);

    if (image) {
      reader.readAsDataURL(image);
    }
  }

  delKeyword(keyword: string) {
    this.fileItem.keywords = this.fileItem.keywords.filter(x => x !== keyword);
    this.deleteKeyword.emit(keyword);
  }

  keywordsChanged() {
    while (this.textContent.includes(',')) {
      this.textContent = this.textContent.replace(',', ' ');
    }
    while (this.textContent.includes(';')) {
      this.textContent = this.textContent.replace(';', ' ');
    }
    while (this.textContent.includes('.')) {
      this.textContent = this.textContent.replace('.', ' ');
    }
    let newKeywords = this.textContent.split(/\s+/);
    newKeywords = newKeywords.filter(p => p !== '');
    newKeywords.forEach(p => {
      if (this.fileItem.keywords != null && this.fileItem.keywords !== undefined
        && !this.fileItem.keywords.includes(p) && this.fileItem.keywords.length < 50) {
        this.fileItem.keywords.push(p);
      }
      if (this.fileItem.keywords === undefined || this.fileItem.keywords == null) {
        this.fileItem.keywords = [p];
      }
    }
    );
    if (this.textContent.length !== 0) {
      this.newKeywords.emit(newKeywords);
    }
    this.textContent = '';
  }

  clearKeywords() {
    this.fileItem.keywords = [];
    this.keywordsClear.emit(true);
  }

  copyKeywords() {
    if (this.fileItem.keywords === undefined || this.fileItem.keywords === null) {
      return ;
    }
    let keywordsString: string;
    this.fileItem.keywords.forEach(p => {
      if (keywordsString === undefined) {
        keywordsString = p;
      } else {
        keywordsString += (' ' + p);
      }
    });
    this.copyMessage(keywordsString);
    this.snackBar.open('Copied', '', {
      duration: 2000
    });
  }

  copyMessage(val: string) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }

  toggle() {
    this.fileItem.isChecked = !this.fileItem.isChecked;
    this.itemCheck.emit(this.fileItem);
  }
  titleChanged() {
    this.itemTitle.emit(this.fileItem);
  }
}
