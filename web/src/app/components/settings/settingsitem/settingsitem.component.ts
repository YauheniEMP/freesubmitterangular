import { Component, OnInit, Input } from '@angular/core';
import { StockData } from 'src/app/models/stockdata';

@Component({
  selector: 'app-settingsitem',
  templateUrl: './settingsitem.component.html',
  styleUrls: ['./settingsitem.component.css']
})
export class SettingsItemComponent implements OnInit {
  @Input() stockItem: StockData;
  constructor() { }

  ngOnInit() {
  }

}
