import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/app/services/account.service';
import { StockData } from 'src/app/models/stockdata';
import { GuardService } from 'src/app/services/guard.service';
import { MatSnackBar } from '@angular/material';
@Component({
  selector: 'app-settingsmain',
  templateUrl: './settingsmain.component.html',
  styleUrls: ['./settingsmain.component.css']
})
export class SettingsMainComponent implements OnInit {
  hidden = true;
  stockData: StockData[] = [];
  currentData: StockData = new StockData();
  constructor(private accountService: AccountService, private guard: GuardService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.initialize();
  }

  async initialize() {
    await this.guard.isAuthenticated().then(res => {
      if (res) {
        this.accountService.stockData().then(data => {
          this.stockData = data.data.sort((a, b) => {
            if (a.stock < b.stock) { return -1; }
            if (a.stock > b.stock) { return 1; }
            return 0;
          });
          this.currentData = this.stockData[0];
        }
        );
        this.hidden = false;
      }
    });
  }
  setCurrentDataItem(item: StockData) {
    this.currentData = item;
  }
  async saveChanges() {
    this.accountService.saveStockData(this.stockData).then(data => {
      if (data) {
        this.snackBar.open('Data has been saved', '', {
          duration: 2000,
        });
      } else {
        this.snackBar.open('Unexpected error', 'Fail', {
          duration: 2000,
        });
      }
    });
  }

}

