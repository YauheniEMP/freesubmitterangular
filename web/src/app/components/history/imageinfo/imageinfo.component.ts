import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ImagesDataService } from 'src/app/services/imagesData.service';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-imageinfo',
  templateUrl: './imageinfo.component.html',
  styleUrls: ['./imageinfo.component.css']
})
export class ImageInfoComponent implements OnInit {

  constructor(private sanitizer: DomSanitizer, private route: ActivatedRoute, private imageData: ImagesDataService,
              private snackBar: MatSnackBar) { }
  title: string;
  keywords: string[] = [];
  name: string;
  imageToShow: any;
  imageToShowSafe: SafeUrl;
  ngOnInit() {
    this.name = this.route.snapshot.paramMap.get('name');
    this.imageData.getPreview(this.name).subscribe(data => {
      this.createImageFromBlob(data);
    });
    this.initialize();
  }

  async initialize() {
   await this.imageData.getTitle(this.name).then( data => this.title = data.title );
   await this.imageData.getKeywords(this.name).then( data =>  this.keywords = data.keywords );
  }

  createImageFromBlob(image: Blob) {
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      this.imageToShow = reader.result;
      this.imageToShowSafe = this.sanitizer.bypassSecurityTrustUrl(this.imageToShow);
    }, false);

    if (image) {
      reader.readAsDataURL(image);
    }
  }

  copyKeywords() {
    if (this.keywords == null) {
      return;
    }
    let keywordsString: string;
    this.keywords.forEach(p => {
      if (keywordsString === undefined) {
        keywordsString = p;
      } else {
        keywordsString += (' ' + p);
      }
    });
    this.copyMessage(keywordsString);
    this.snackBar.open('Copied', '', {
      duration: 2000
    });
  }

  copyMessage(val: string) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }
}
