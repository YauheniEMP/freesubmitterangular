import { Component, OnInit } from '@angular/core';
import { ImagesDataService } from 'src/app/services/imagesData.service';
import { FilesService } from 'src/app/services/files.service';
import { Image } from 'src/app/models/image';
import { GuardService } from 'src/app/services/guard.service';

@Component({
  selector: 'app-submitted',
  templateUrl: './submitted.component.html',
  styleUrls: ['./submitted.component.css']
})
export class SubmittedComponent implements OnInit {
  lastImages: Image[] = [];
  hidden = true;
  page = 1;
  pages: number;
  constructor(private imagesService: ImagesDataService, private filesService: FilesService, private guard: GuardService) { }

  ngOnInit() {
    this.initialize();
  }

  initialize() {
    this.guard.isAuthenticated().then(res => {
      if (res) {
        this.imagesService.getLastImages(1).then(data => {
          this.lastImages = data.result;
          this.pages = Math.floor(data.totalCount / 50) + 1;
          if (data.result != null && data.result.length !== 0) {
            this.lastImages.forEach(x => x.isChecked = false);
          }
          this.hidden = false;
        });
      }
    });
  }
  async deleteFiles() {
    const ids: string[] = [];
    this.lastImages.filter(x => x.isChecked === true).forEach(p => ids.push(p.id));
    await this.filesService.deleteImages(ids);
    this.lastImages = this.lastImages.filter(x => x.isChecked === false);

  }
  itemFlagChanged(e: Image) {
    this.lastImages.find(x => x.id === e.id).isChecked = e.isChecked;
  }

  pagesArray() {
    const arr = [];
    for (let i = 1; i <= this.pages; i++) {
      arr.push(i);
    }
    return arr;
  }

  paginator(page: number) {
    if (page !== this.page) {
      this.imagesService.getLastImages(page, 50).then(data => {
        if (data.result != null && data.result.length !== 0) {
          this.lastImages = data.result;
          this.pages = Math.floor(data.totalCount / 50) + 1;
          this.page = page;
          this.lastImages.forEach(x => x.isChecked = false);
        }
        this.hidden = false;
      });
    }
  }
}

