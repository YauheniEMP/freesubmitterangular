import { Component, OnInit } from '@angular/core';
import { ImagesDataService } from 'src/app/services/imagesData.service';
import { Image } from 'src/app/models/image';
import { GuardService } from 'src/app/services/guard.service';
@Component({
  selector: 'app-inprocess',
  templateUrl: './inprocess.component.html',
  styleUrls: ['./inprocess.component.css']
})
export class InProcessComponent implements OnInit {

  lastImages: Image[] = [];
  hidden = true;
  constructor(private imagesService: ImagesDataService, private guard: GuardService) { }
  ngOnInit() {
    this.initialize();
  }

  async initialize() {
    await this.guard.isAuthenticated().then(res => {
      if (res) {
        this.imagesService.getProcessingImages().then(data => {
          if (data.result != null && data.result.length !== 0) {
            this.lastImages = data.result;
          }
          this.hidden = false;
        });
      }
    });
  }
}
