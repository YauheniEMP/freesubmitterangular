import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Image } from 'src/app/models/image';
import { Router } from '@angular/router';
import { FilesService } from 'src/app/services/files.service';

@Component({
  selector: 'app-historyitem',
  templateUrl: './historyitem.component.html',
  styleUrls: ['./historyitem.component.css']
})
export class HistoryitemComponent implements OnInit {
  @Input() item: Image;
  date: string;
  @Output() itemFlag: EventEmitter<Image> = new EventEmitter();
  @Input() isInprocessRoute: boolean;
  constructor(private router: Router, private filesService: FilesService) { }

  ngOnInit() {
    const currentDate = new Date(this.item.submitDateTime);
    this.date = currentDate.toDateString();
  }

  resubmitFile(stock: string) {
    this.item.statusList.find(p => p.stock === stock).status = 'InProcess';
    this.filesService.resubmitFile(this.item.id, stock);
  }
  imageDetails() {
    this.router.navigate(['/imageinfo', this.item.name]);
  }
  onChangeFlag() {
    this.itemFlag.emit(this.item);
  }

  getShortName(name: string) {
    if (name === 'ShutterStock') {
      return 'Shutter';
    }
    if (name === 'AdobeStock') {
      return 'Adobe';
    }
    if (name === 'Depositphotos') {
      return 'Deposit';
    }
    if (name === 'VectorStock') {
      return 'Vector';
    }
    if (name === 'DreamsTime') {
      return 'Dreams';
    }
    if (name === 'RoyaltyFree') {
      return '123RF';
    }
    return name;
  }
}
