import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AccountService } from 'src/app/services/account.service';
@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {
  resetForm: FormGroup;
  constructor(private fb: FormBuilder, private router: Router, private accountService: AccountService
              ) { }
  code: string;
  email: string;
  ngOnInit() {
    this.email = this.router.routerState.snapshot.root.queryParams.email;
    this.code = this.router.routerState.snapshot.root.queryParams.code;
    if (this.code === undefined || this.code == null) {
      this.router.navigate(['/recovery']);
    }
    this.resetForm = this.fb.group({
      Password: ['', [Validators.required, Validators.minLength(4)]],
      ConfirmPassword: ['', Validators.required]
    }, { validator: this.comparePasswords });
    this.resetForm.reset();
  }
  comparePasswords(fg: FormGroup) {
    const confirmPasswordCtrl = fg.get('ConfirmPassword');
    if (confirmPasswordCtrl.errors == null || 'passwordMismatch' in confirmPasswordCtrl.errors) {
      if (confirmPasswordCtrl.value !== fg.get('Password').value) {
        confirmPasswordCtrl.setErrors({ passwordMismatch: true });
      } else {
        confirmPasswordCtrl.setErrors(null);
      }
    }
  }

  async changePassword() {
    await this.accountService.setNewPassword(
      this.email, this.code, this.resetForm.get('Password').value, this.resetForm.get('ConfirmPassword').value)
      .then(data => {
        if (data.result === true) {
          this.router.navigate(['/login']).then(() => {
            // this.toastrService.info('Password changed', 'Success');
          });
        }
      });
  }
}
