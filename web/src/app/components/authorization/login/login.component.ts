import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AccountService } from 'src/app/services/account.service';
import { User } from 'src/app/models/user';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { AuthdataService } from 'src/app/services/authdata.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  nameSubject = new Subject<any>();
  loginForm: FormGroup;
  constructor(private fb: FormBuilder, private accountService: AccountService,
              private router: Router, private auth: AuthdataService) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      Email: [],
      Password: []
    });
  }

  async loginUser() {
    const user: User = { email: this.loginForm.value.Email, password: this.loginForm.value.Password };
    await this.accountService.login(user).then(data => {
      if (data.result) {
        this.auth.setData(user.email, true, data.isConfirmed);
        this.router.navigate(['/']);
      } else {
        // this.toastrService.danger('Email or password wrong', 'Error');
      }
    }
    );

  }
}
