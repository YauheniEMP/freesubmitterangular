import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AccountService } from 'src/app/services/account.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  constructor(private fb: FormBuilder, private accountService: AccountService) { }

  ngOnInit() {
    this.registerForm = this.fb.group({
      Email: ['', [Validators.required, Validators.email]],
      Password: ['', [Validators.required, Validators.minLength(4)]],
      ConfirmPassword: ['', Validators.required]
    }, { validator: this.comparePasswords });
    this.registerForm.reset();
  }
  comparePasswords(fg: FormGroup) {
    const confirmPasswordCtrl = fg.get('ConfirmPassword');
    if (confirmPasswordCtrl.errors == null || 'passwordMismatch' in confirmPasswordCtrl.errors) {
      if (confirmPasswordCtrl.value !== fg.get('Password').value) {
        confirmPasswordCtrl.setErrors({ passwordMismatch: true });
      } else {
        confirmPasswordCtrl.setErrors(null);
      }
    }
  }

  async regusterUser() {
    if (this.registerForm.get('Email').errors != null) {
      // this.toastrService.danger('Email not valid', 'Error:');
    }
    if (this.registerForm.get('Password').errors != null) {
      // this.toastrServikce.danger('Password not valid', 'Error:');
    }
    if (this.registerForm.get('ConfirmPassword').errors != null) {
      // this.toastrService.danger('ConfirmPassword not valid', 'Error:');
    }
    const user = new RegisterUser();
    user.email = this.registerForm.value.Email;
    user.password = this.registerForm.value.Password;
    user.confirmPassword = this.registerForm.value.ConfirmPassword;
    await this.accountService.register(user).then(data => {
      if (data.result) {
        // this.toastrService.info(`Details sent to ${user.email}`, 'Verify your email');
      } else {
        // this.toastrService.danger(data.error, 'Error:');
      }
    });
  }
}
export class RegisterUser {
  email: string;
  password: string;
  confirmPassword: string;
}

