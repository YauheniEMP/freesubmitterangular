import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/app/services/account.service';

@Component({
  selector: 'app-recoverypassword',
  templateUrl: './recoverypassword.component.html',
  styleUrls: ['./recoverypassword.component.css']
})
export class RecoverypasswordComponent implements OnInit {
  msgShow = false;
  email: string;
  error = '';
  errorshow = false;
  constructor(private accountService: AccountService) { }

  ngOnInit() {
  }
  onsubmit() {
    this.accountService.sendRecoveryEmail(this.email).then(data => {
      if (data.result) {
        this.msgShow = true;
      } else {
        this.error = data.error;
        this.errorshow = true;
      }
    });
  }
}
