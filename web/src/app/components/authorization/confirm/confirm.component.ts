import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from 'src/app/services/account.service';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.css']
})
export class ConfirmComponent implements OnInit {
  email: string;
  code: string;
  constructor(private router: Router, private accountService: AccountService) { }

  ngOnInit() {
    this.initialize();
  }

  async initialize() {
    this.email = this.router.routerState.snapshot.root.queryParams.email;
    this.code = this.router.routerState.snapshot.root.queryParams.code;
    await this.accountService.confirmEmail(this.email, this.code).then(data => {
      if (data) {
        this.router.navigate(['/upload']).then(() => {
          window.location.reload();
        });
      }
    });
  }
}
