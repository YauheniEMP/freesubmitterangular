import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthdataService } from 'src/app/services/authdata.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
 
  subscription: Subscription;
  isAuth: boolean;
  constructor(private auth: AuthdataService) { }

  ngOnInit(): void {
    this.subscription = this.auth.getData().subscribe(data => {
      this.isAuth = data.isAuth;
    });
  }

}
