import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { FilesService } from 'src/app/services/files.service';
import { HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-uploaditem',
  templateUrl: './uploaditem.component.html',
  styleUrls: ['./uploaditem.component.css']
})
export class UploadItemComponent implements OnInit {

  @Input() file: File;
  @Output() fileToEmit: EventEmitter<File> = new EventEmitter();
  mouseover: boolean;
  constructor() { }

  ngOnInit() {
  }

  delFile() {
    this.fileToEmit.emit(this.file);
  }

  onMouseOver() {
this.mouseover = true;
  }

  onMouseOut() {
this.mouseover = false;
  }

}
