import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { FilesService } from 'src/app/services/files.service';
import { UploadingImage } from 'src/app/models/uploadingImage';

@Component({
  selector: 'app-uploading',
  templateUrl: './uploading.component.html',
  styleUrls: ['./uploading.component.css']
})
export class UploadingComponent implements OnInit {
  loadingfiles: UploadingImage[] = [];

  constructor(private filesService: FilesService) { }

  getLoadingFiles() {
    this.filesService.loadingFiles().then(res => {
      res.forEach(p => {
        this.loadingfiles.push(p);
      });
    });
  }

  ngOnInit() {
    this.getLoadingFiles();
  }
}
