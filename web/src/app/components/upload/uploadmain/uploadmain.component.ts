import { Component, OnInit, EventEmitter, Output, Input, ViewChild } from '@angular/core';
import { GuardService } from 'src/app/services/guard.service';
import { FilesService } from 'src/app/services/files.service';
import { UploadingComponent } from '../uploading/uploading.component';
import { UploadingImage } from 'src/app/models/uploadingImage';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-uploadmain',
  templateUrl: './uploadmain.component.html',
  styleUrls: ['./uploadmain.component.css']
})
export class UploadMainComponent implements OnInit {
  @ViewChild(UploadingComponent, { static: false}) private uploading: UploadingComponent;
  currentfiles: File[] = [];
  count = 0;
  hidden = true;

  @Input() refreshLoadingFilesCompleteHandrer: boolean;
  constructor(private guard: GuardService, private filesService: FilesService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.initialize();
  }

  initialize() {
    this.guard.isAuthenticated().then(res => {
        if (res) {
          this.hidden = false;
        }
    });
  }

  onChange(files: any) {
    for (const file of files) {
      if (!this.currentfiles.find(x => x.name === file.name)) {
        this.currentfiles.push(file);
        this.count = this.count + 1;
      }
    }
    this.currentfiles.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));
  }

  delItem(file: any) {
    this.currentfiles = this.currentfiles.filter((t) => t.name !== file.name);
    this.count = this.count - 1;
  }

  clearFilesList() {
    this.currentfiles = [];
    this.count = 0;
  }

  uploadFiles() {
    this.currentfiles.forEach(x => {
      const item = new UploadingImage();
      item.name = x.name;
      this.uploading.loadingfiles.push(item);
    });
    this.filesService.uploadFiles(this.currentfiles).then();
    this.snackBar.open('Files uploading', '', {
      duration: 2000
    });
    this.clearFilesList();
  }
}
